require "test_helper"

class WpAdminControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get wp_admin_index_url
    assert_response :success
  end
end
