#!/usr/bin/env nix-shell
#!nix-shell -i bash -p ruby_3_0 bundler bundix

rm -f gemset.nix Gemfile.lock
BUNDLE_FORCE_RUBY_PLATFORM=1 bundle lock
bundix --ruby=ruby_3_0