Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  # we love web crawler bots
  root to: "home#index"

  get "/ajaproxy/proxy.php" , to: "wp_admin#index"
  get "/bitrix/admin/index.php" , to: "wp_admin#index"
  get "/magmi/web/magmi.php" , to: "wp_admin#index"
  get "/wp-admin/admin-ajax.php" , to: "wp_admin#index"
  get "/wp-admin/includes/themes.php" , to: "wp_admin#index"
  get "/wp-admin/options-link.php" , to: "wp_admin#index"
  get "/wp-admin/post-new.php" , to: "wp_admin#index"
  get "/wp-login.php" , to: "wp_admin#index"
  get "/xmlrpc.php" , to: "wp_admin#index"
  get "/wp_admin", to: "wp_admin#index"


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
